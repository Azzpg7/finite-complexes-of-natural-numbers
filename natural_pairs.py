print("input n: ", end='')
n = int(input())

cnt = 0
current_sum = 0
while cnt != n:
    left = 0
    right = current_sum - left
    while left <= current_sum and cnt != n:
        print(f"n = {cnt}: ({left}, {right})")
        cnt += 1
        left += 1
        right -= 1
    current_sum += 1
