print("input n: ", end='')
n = int(input())


for i in range(1, n + 1):
    x = i
    p = []
    summa = 0
    power = 0
    while x > 0:
        if x & 1:
            p_i = power + 1 - summa  # p_1 + p_2 + ... + p_i - 1 = power
            p.append(p_i)
            summa += p_i
        power += 1
        x >>= 1
    print(f"n = {i} <=> ({','.join(list(map(str, p)))})")
