print("input n: ", end='')
n = int(input())

cnt = 0
current_sum = 2
while cnt != n:
    left = 1
    right = current_sum - left
    while left < current_sum and cnt != n:
        print(f"n = {cnt + 1}: ({left}, {right})")
        cnt += 1
        left += 1
        right -= 1
    current_sum += 1
